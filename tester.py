#!/usr/bin/python3
import solver

def test_example1():
    f = "./tests/example1.txt"
    rows,columns,grid,words = solver.fileReader(f)
    assert solver.gridSolver(rows,columns,grid,words) == ["HELLO 0:0 4:4","BYE 1:3 1:1","GOOD 4:0 4:3"]

def test_example2():
    f = "./tests/example2.txt"
    rows,columns,grid,words = solver.fileReader(f)
    assert solver.gridSolver(rows,columns,grid,words) == ["ABC 0:0 0:2","AEI 0:0 2:2"]

if __name__ == "__main__":
    test_example1()
    print("Example 1 passed")
    test_example2()
    print("Example 2 passed")
