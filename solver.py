#!/usr/bin/python3
import sys

def checkWord(current_row,current_column,complete_grid,target_word,vector,current_letter=0,found_letters=""):
    # Check letter at current position
    if complete_grid[current_row][current_column] == target_word[current_letter]:
        found_letters = found_letters + complete_grid[current_row][current_column]
    else:
        return False,0,0

    # If word matches
    if found_letters == target_word:
        return True,current_row,current_column
    else:
        # Check if next vector position is valid
        try:
            temp = complete_grid[current_row+vector[0]][current_column+vector[1]]
            letter = current_letter + 1
            return checkWord(current_row+vector[0],current_column+vector[1],complete_grid,target_word,vector,current_letter=letter,found_letters=found_letters)
        except Exception as e:
            return False,0,0



def gridSolver(rows,columns,grid,words):
    vectors = [
            [0,1], # right
            [1,0], # down
            [0,-1], # left
            [-1,0], # up
            [-1,-1], # NW
            [-1,1], # NE
            [1,-1], # SW
            [1,1], # SE
            ]
    solutions = []
    for row in range(0,rows):
        for col in range(0,columns):
            for word in words.keys():
                # Don't bother checking for found words
                if not words[word]:
                    for v in vectors:
                        found,end_row,end_col = checkWord(row,col,grid,word,v)
                        if found:
                            solutions.append('{0} {1}:{2} {3}:{4}'.format(word,row,col,end_row,end_col))
                            words[word] = True
    return solutions

# Assume file is properly formatted
def fileReader(inputFile):
    f = open(inputFile,'r')
    #AxB
    first_line = f.readline()
    rows = int(first_line.split('x')[0])
    columns = int(first_line.split('x')[1])

    # Check if part of grid or answers
    rest_of_file = f.readlines()
    grid = []
    words = {}
    for line in rest_of_file:
        if " " in line:
            grid.append(line.rstrip().split(" "))
        else:
            words[line.rstrip()] = False

    return rows,columns,grid,words

def main():
    if len(sys.argv) < 2:
        print("Call program as: solver.py <path to input file>")
        sys.exit(1)
    inputFile = sys.argv[1]
    rows,columns,grid,words = fileReader(inputFile)
    solutions = gridSolver(rows,columns,grid,words)
    print("\n".join(solutions))

if __name__ == "__main__":
    main()
